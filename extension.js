// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
function activate(context) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "vscode-touchbar" is now active!');
    // The commandId parameter must match the command field in package.json
    let integratedTerminal = vscode.commands.registerCommand('extension.showTerminal', function () {
        // Toggle Integrated Terminal
        vscode.commands.executeCommand('workbench.action.terminal.toggleTerminal')
    });

    let splitWindowRight = vscode.commands.registerCommand('extension.splitRight', () => {
        vscode.commands.executeCommand('workbench.action.splitEditorRight')
    })

    let splitWindowDown = vscode.commands.registerCommand('extension.splitDown', () => {
        vscode.commands.executeCommand('workbench.action.splitEditorDown')
    })
    
    let findInFiles = vscode.commands.registerCommand('extension.findInFiles', () => {
        vscode.commands.executeCommand('workbench.action.findInFiles')
    })

    context.subscriptions.push(integratedTerminal, splitWindowDown, splitWindowRight, findInFiles);
}
exports.activate = activate;

// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;