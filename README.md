## Features

Add shortcuts to your Macbook Pro touchbar. No need to memorize those long split commands and terminal toggles.

![Split editor down](https://bytebucket.org/sebastian_otarola_ddb/vs_code_touchbar/raw/c67ef461e454f58edb03220194bf1f0946965c49/images/screen_split_down.png) **Split Editor Down**

![Split Editor Right](https://bytebucket.org/sebastian_otarola_ddb/vs_code_touchbar/raw/c67ef461e454f58edb03220194bf1f0946965c49/images/screen_split_right.png) **Split Editor Right**

![Toggle Terminal](https://bytebucket.org/sebastian_otarola_ddb/vs_code_touchbar/raw/c67ef461e454f58edb03220194bf1f0946965c49/images/terminal.png) **Toggle Terminal**

![Find in files](https://bytebucket.org/sebastian_otarola_ddb/vs_code_touchbar/raw/c67ef461e454f58edb03220194bf1f0946965c49/images/find_in_files.png) **Find in files**

## Requirements

A Touchbar on the new MacBook Pro.

## Release Notes

First inital release with just the most basics shortcuts you shouldn't have to remember.

### 1.0.0

Initial release of SmokingSlims Touchbar Extension.

>***
>### Creators note
>Created by Sebastian Otarola, @SmokingSlim (twitter) <br>
> ***

**Enjoy!**
